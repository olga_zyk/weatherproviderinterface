<?php

namespace Nfq\Weather;

class Location
{
    /**
     * @var float
     * @var float
     */
    public $lat;
    public $lon;

    public function __construct(float $lat, float $lon)
    {
        $this->lat = $lat;
        $this->lon = $lon;
    }

}