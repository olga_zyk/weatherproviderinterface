<?php

use Nfq\Weather\FakeWeatherProvider;
use Nfq\Weather\OpenWeatherMapProvider;
use Nfq\Weather\DelegatingWeatherProvider;
use Nfq\Weather\YahooWeatherProvider;


require __DIR__ . '/vendor/autoload.php';

$lat = 128;
$lon = 33;

$newFake = new FakeWeatherProvider();
$provider = new DelegatingWeatherProvider($newFake);
//$provider->addLocation(54.6872, 25.2797);
$provider->addLocation($lat, $lon)
    ->fetchWeather();


$OpenWeatherMap = new OpenWeatherMapProvider('6a7eb7000f181966fd0b0da46914b820');
$provider = new DelegatingWeatherProvider($OpenWeatherMap);

$YahooWeather = new YahooWeatherProvider();
$provider = new DelegatingWeatherProvider($YahooWeather);







