<?php

namespace Nfq\Weather;


class YahooWeatherProvider implements WeatherProviderInterface
{
    public function fetch(Location $location): Weather
    {
        $BASE_URL = "http://query.yahooapis.com/v1/public/yql";
        $yql_query = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"({$location->lat},{$location->lon})\") and u = 'c'";
        $yql_query_url = $BASE_URL . "?q=" . urlencode($yql_query) . "&format=json";
        $json = file_get_contents($yql_query_url);
        // Convert JSON to PHP object
        $data = json_decode($json);
//        var_dump($data);

        return new Weather ($data->query->results->channel->location->city, $data->query->results->channel->item->condition->temp, $data->query->results->channel->wind->speed);
    }
}