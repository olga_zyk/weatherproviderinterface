<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 2018-04-08
 * Time: 22:40
 */

namespace Nfq\Weather;


class FakeWeatherProvider implements WeatherProviderInterface
{
    public function fetch(Location $location): Weather
    {
        return new Weather('Varena', '8', '11');
    }

}