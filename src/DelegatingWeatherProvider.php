<?php

namespace Nfq\Weather;


class DelegatingWeatherProvider
{
    private $provider;
    private $locations = array();
    private $weatherArray = array();

    public function __construct(WeatherProviderInterface $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param float $lat
     * @param float $lon
     * @return $this
     */
    public function addLocation(float $lat, float $lon)
    {
        $this->locations[] = new Location($lat, $lon);
        var_dump($this->locations);
        return $this;
    }

    public function fetchWeather()
    {
        $weatherArray = array();
        foreach ($this->locations as $location) {
            try {
                $weatherArray[] = $this->provider->fetch($location);
            } catch (Exception $exception) {

            }

        }

        $this->weatherArray = $weatherArray;
    }

    public function printWeather()
    {
        echo $weather->name . PHP_EOL
            . 'Temperature: ' . $weather->temperature . '°C' . PHP_EOL
            . 'Wind: ' . $weather->wind . ' m/s' . PHP_EOL;
    }

}