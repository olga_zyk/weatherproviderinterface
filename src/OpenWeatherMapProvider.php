<?php

namespace Nfq\Weather;


class OpenWeatherMapProvider implements WeatherProviderInterface
{
    private $key;

    public function __construct($key)
    {
        $this->key = $key;
    }

    public function fetch(Location $location): Weather
    {

        $url = "https://api.openweathermap.org/data/2.5/weather?lat={$location->lat}&lon={$location->lon}&units=metric&appid={$this->key}";
        $json = file_get_contents($url);
        $data = json_decode($json);
        return new Weather($data->name, $data->main->temp, $data->wind->speed);

    }


}