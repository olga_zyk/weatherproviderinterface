<?php

namespace Nfq\Weather;


class Weather
{
    public $name;
    public $temperature;
    public $wind;

    /**
     * Weather constructor.
     * @param $name
     * @param $temperature
     * @param $wind
     */
    public function __construct($name, $temperature, $wind)
    {
        $this->name = $name;
        $this->temperature = $temperature;
        $this->wind = $wind;
    }


}